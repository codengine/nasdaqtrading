import requests
from datetime import datetime
import pymysql.cursors
import copy
import json
import xml.etree.ElementTree as ET
from ftplib import FTP


HOST_NAME = 'localhost'
USERNAME = 'root'
PASSWORD = 'root'
DATABASE_NAME = 'clk'
DB_TABLE_NAME = 'clk_data'


def get_page(url):
    try:
        r = requests.get(url)
        if r.status_code == 200:
            return r.content.decode('latin1')
        return None
    except Exception as exp:
        return None


def parse_clk(content):
    try:
        root = ET.fromstring(content)
        return root[0][0].text
    except Exception as exp:
        return None


def read_file(file_name):
    contents = []
    with open(file_name, "r") as f:
        lines = f.readlines()
        for i, line in enumerate(lines):
            if i > 0:
                line = line.strip()
                line_split = line.split('|')
                contents += [line_split]
    return contents


def read_symbols():
    print("Reading symbols")
    file1 = 'nasdaqtraded.txt'
    contents1 = read_file(file1)

    contents1 = [l[1] for l in contents1 if l]

    file2 = 'otherlisted.txt'
    contents2 = read_file(file2)

    contents2 = [l[0] for l in contents2 if l and len(l) > 0]

    data = contents1 + contents2

    json_data = {}

    for l in data:
        json_data[l] = False

    with open('symbols.json', 'w') as f:
        json.dump(json_data, f)

    print("Symbol read done!")


def save_json(json_data, file_name='symbols.json'):
    try:
        with open(file_name, 'w') as f:
            json.dump(json_data, f)
        return True
    except Exception as exp:
        print("Exception when saving the json data: %s" % str(exp))
        return False


def download_clk():
    print("Downloading CLK")
    json_data = {}
    try:
        with open('symbols.json', 'r') as f:
            json_data = json.loads(f.read()) or {}
    except Exception as exp:
        pass

    original_json = copy.deepcopy(json_data)

    considered_keys = filter(lambda x: not json_data[x], json_data)

    considered_keys = sorted(considered_keys)

    # print(considered_keys)

    i = 0

    for key in considered_keys:
        if not key:
            continue
        print("Downloading CLK for symbol %s" % key)
        url = "https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&output=xml&CIK=%s" % key
        print("Entering URL: %s" % url)
        content = get_page(url)
        if content:
            clk = parse_clk(content)
            if clk:
                original_json[key] = {'clk': clk}
                saved = save_json(original_json)
                if saved:
                    print("Updated")
                else:
                    print("Update failed")
        i += 1
        # if i == 100:
        #     break
    print("CLK download done!")


def connect_to_mysql(host=HOST_NAME, user=USERNAME, password=PASSWORD, db=DATABASE_NAME):
    try:
        connection = pymysql.connect(host=host,
                                     user=user,
                                     password=password,
                                     db=db,
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
        return connection
    except Exception as exp:
        print("Exception connecting database. Exception: %s" % str(exp))
        return None


def create_db_schema(table_name=DB_TABLE_NAME):
    schema = """
    CREATE TABLE IF NOT EXISTS %s (
       id int not null auto_increment primary key,
       symbol varchar(200) NOT NULL,
       clk varchar(1000) NOT NULL,
       date_created datetime DEFAULT CURRENT_TIMESTAMP 
    )
    """ % table_name
    return schema


def prepare_insert_query(table_name=DB_TABLE_NAME, column_names=['symbol', 'clk'], data=[]):
    if not data:
        return None

    now_time = datetime.utcnow()

    now_time_str = now_time.strftime('%Y-%m-%d %H:%M:%S.%f')

    values = []

    for k, v in data:
        value = "('%s', '%s')" % (k, v)
        values += [value]

    values_str = ', '.join(values)

    column_names_str = ', '.join(column_names)

    query_string = 'INSERT INTO %s(%s) VALUES %s' % (table_name, column_names_str, values_str)

    return query_string


def execute_query(db_connection, query):
    try:
        with db_connection.cursor() as cursor:
            cursor.execute(query)
        db_connection.commit()
        return True
    except Exception as exp:
        print("Exception: %s" % str(exp))
        return None


def download_nasdaq_symbols(host_name='ftp.nasdaqtrader.com'):
    print("Downloading Symbols")
    # ftp://ftp.nasdaqtrader.com/SymbolDirectory/nasdaqtraded.txt
    # ftp://ftp.nasdaqtrader.com/SymbolDirectory/otherlisted.txt
    ftp = FTP(host_name)
    ftp.login()
    ftp.cwd('SymbolDirectory')

    def writeline(line):
        file.write(line + "\n")

    file = open('nasdaqtraded.txt', "w")
    ftp.retrlines("retr " + 'nasdaqtraded.txt', writeline)

    file = open('otherlisted.txt', "w")
    ftp.retrlines("retr " + 'otherlisted.txt', writeline)

    print("Symbols downloaded")


def perform_db_operation():
    print("Performing DB Operation")
    db_connection = connect_to_mysql()
    if db_connection:
        print("DB Connected")
    else:
        print("DB Connection failed. Exiting...")
        exit()
    db_table = create_db_schema()
    table_created = execute_query(db_connection, db_table)
    if table_created:
        print("DB Table created")
    else:
        print("DB Table creation failed. Exiting...")
        exit()

    json_data = {}
    try:
        with open('symbols.json', 'r') as f:
            json_data = json.loads(f.read()) or {}
    except Exception as exp:
        pass

    considered_keys = filter(lambda x: json_data[x], json_data)

    considered_keys = sorted(considered_keys)

    chunk_size = 1000

    for i in range(0, len(considered_keys), chunk_size):
        start = i
        end = i + chunk_size

        small_chunk_data = considered_keys[start: end]

        upload_rows = []

        for k in small_chunk_data:
            if json_data.get(k):
                upload_rows += [(k, json_data.get(k)['clk'])]

        insert_query = prepare_insert_query(data=upload_rows)

        inserted = execute_query(db_connection, insert_query)
        if inserted:
            print("Items Inserted %s" % len(small_chunk_data))
        else:
            print("Item insert Failed")
    print("DB Save done!")


if __name__ == "__main__":
    download_nasdaq_symbols()

    read_symbols()

    download_clk()

    perform_db_operation()



